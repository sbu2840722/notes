# Density matrix


## Unitary freedom in representation
Let $\mathcal H$ be a finite dimensional Hilbert space. Let $S_1=\{\ket{\psi_i}\}_{i=1}^m$ and $S_2=\{\ket{\phi_i}\}_{i=1}^n$ be a set $m$ and $n$ states in the Hilbert space respectively, where $m$ not necessarily equal to $n$. If $S_1$ and $S_2$ produce the same density matrix 
$$\rho = \sum_{i=1}^m p_i \ket{\psi_i}\bra{\psi_i} = \sum_{j=1}^n p_j' \ket{\phi_j}\bra{\phi_j}$$
Then there exist a matrices $\,\mathcal T_1 \in \mathbb C^{n \times m}$ and $\mathcal T_2 \in \mathbb C^{m\times n}$ that maps vectors from $S_1'=\{\ket{\tilde \psi_i}=\sqrt{p_i}\ket{\psi_i}\}_{i=1}^m$ to $S_2' = \{\ket{\tilde \phi_i}=\sqrt{p_j'}\ket{\phi_i}\}_{i=1}^n$ and vice versa. Furthermore, these maps are "unitary" in the sense that $\mathcal T_1^\dagger \mathcal T_1$ and $\mathcal T_1 \mathcal T_1^\dagger$ are identity operators in the subspace spanned bt $S_1'$ and $S_2'$. 

**Proof:** 
This is a taken from Nielsen and Chuang. I have made some of the arguments more explicit and clarify some confusing parts.

Step 1: $\dim \text{Span}(S_1') = \dim \text{Span}(S_2')=\text{rank}(\rho)$

This is obvious. Let $M_1=\left(\ket{\psi_1}|\,\ket{\psi_2}\,|\cdots|\,\ket{\psi_m}\right)$ and $M_2=\left(\ket{\phi_1}|\,\ket{\phi_2}\,|\cdots|\,\ket{\phi_n}\right)$. Then it is clear that $\rho = M_1 M_1^\dagger = M_2 M_2^\dagger$. So the rank of $M_1,M_2,\rho$ are all equal. We will let $r$ be the rank of $\rho$. In fact, the column space of $\rho$ is equal to that of $\text{Span}(S_1')$ and $\text{Span}(S_2')$. 


Step 2: Since $\rho$ is hermitian and positive semi-definite. We can diagonalize $\rho$ with respect to some orthonormal basis $\{\ket{k}\}_{k=1}^r$, where $r$ is as defined above,
$$\rho = \sum_{k=1}^{r} \lambda_k \ket{k}\bra{k}$$
Therefore, we can expand $$\ket{\tilde \psi_i}=\sum_{k=1}^{r} c_{ik}\ket{\tilde k}$$
where $c_{ik}= \braket{k|\tilde \psi_i}/\sqrt{\lambda_k}$. If we group $c_{ik}$ into a matrix $\mathbf C \in \mathbb C^{m\times r}$, one can easily check that $\mathbf C^\dagger \mathbf C = \mathbf 1_{r}$. We can do the same for $\ket{\tilde \phi_i}$ and construct a matrix $\mathbf D \in \mathbb C^{n \times r}$ and $\mathbf D^\dagger \mathbf D = \mathbf 1_{r}$  

Step 3: We now construct the matrix $\mathbf T_1=\mathbf D \mathbf C^\dagger \in \mathbb C^{n\times m}$. One can easily check that $$\ket{\tilde \phi_i} = \sum_{j=1}^mt^{(1)}_{ij}\ket{\tilde \psi_j}$$ for all $1\leq i \leq n$. Furthermore, $\mathbf T_1^\dagger\mathbf T_1 = \mathbf C \mathbf C^\dagger \in \mathbb C^{m\times m}$ and $\mathbf T_1 \mathbf T_1^\dagger = \mathbf D \mathbf D^\dagger \in \mathbb C^{n\times n}$. The matrices $\mathbf C\mathbf C^\dagger$ and $\mathbf D \mathbf D^\dagger$ are identity operators in $\text{Span}(S_1')$ and $\text{Span}(S_2')$ respectively because $\mathbf C\mathbf C^\dagger \mathbf C = \mathbf C$ and $\mathbf D\mathbf D^\dagger \mathbf D = \mathbf D$. A similar map $\mathbf T_2$ can be constructed for $\ket{\tilde \phi_i}$.
