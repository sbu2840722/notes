# Separability of composite states

Separable states are composite states that are essentially classical. In other words, two parts of the separable states are either completely independent, or are correlated only classically. Formally, a composite state is said to be separable if it is a convex combination of separable density operators 
$$\sum_{z}p(z) \,\rho_z\otimes \sigma_z $$
where $\text{Tr}(\rho_z)=\text{Tr}(\sigma_z)=1$